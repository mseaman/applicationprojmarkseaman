﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransportProj;

namespace UnitTest
{
	[TestClass]
	public class PointUnitTest
	{
		[TestMethod]
		public void TestPointsAreEqual()
		{
			Point p1 = new Point(5, 5);
			Point p2 = new Point(5, 5);
			Assert.AreEqual(p1, p2);
			
			Assert.IsTrue(p1.Equals(p2));

		}

	}
}
