﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransportProj;

namespace UnitTest
{
	[TestClass]
	public class CityPositionUnitTest
	{
		[TestMethod]
		public void TestCanMoveUp()
		{
			City city = new City(10, 10);
			var car = city.AddCarToCity(0, 9, "sedan", 1);

			Assert.IsFalse(city.CityPosition.CanMoveUp(car, null));

			var car2 = city.AddCarToCity(0, 8, "racecar", 2);
			Assert.IsFalse(city.CityPosition.CanMoveUp(car2, null));

			var car3 = city.AddCarToCity(0, 7, "racecar", 2);
			Assert.IsTrue(city.CityPosition.CanMoveUp(car3, null));

			var car4 = city.AddCarToCity(0, 0, "racecar", 2);
			Assert.IsTrue(city.CityPosition.CanMoveUp(car4, null));
		}

		[TestMethod]
		public void TestCanMoveDown()
		{
			City city = new City(10, 10);
			var car = city.AddCarToCity(0, 0, "sedan", 1);

			Assert.IsFalse(city.CityPosition.CanMoveDown(car, null));

			var car2 = city.AddCarToCity(0, 1, "racecar", 2);
			Assert.IsFalse(city.CityPosition.CanMoveDown(car2, null));

			var car3 = city.AddCarToCity(0, 2, "racecar", 2);
			Assert.IsTrue(city.CityPosition.CanMoveDown(car3, null));

			var car4 = city.AddCarToCity(0, 5, "racecar", 2);
			Assert.IsTrue(city.CityPosition.CanMoveDown(car4, null));
		}

		[TestMethod]
		public void TestCanMoveRight()
		{
			City city = new City(10, 10);
			var car = city.AddCarToCity(9, 0, "sedan", 1);

			Assert.IsFalse(city.CityPosition.CanMoveRight(car, null));

			var car2 = city.AddCarToCity(8, 0, "racecar", 2);
			Assert.IsFalse(city.CityPosition.CanMoveRight(car2, null));

			var car3 = city.AddCarToCity(7, 0, "racecar", 2);
			Assert.IsTrue(city.CityPosition.CanMoveRight(car3, null));

			var car4 = city.AddCarToCity(0, 0, "racecar", 2);
			Assert.IsTrue(city.CityPosition.CanMoveRight(car4, null));
		}

		[TestMethod]
		public void TestCanMoveLeft()
		{
			City city = new City(10, 10);
			var car = city.AddCarToCity(0, 0, "sedan", 1);

			Assert.IsFalse(city.CityPosition.CanMoveLeft(car, null));

			var car2 = city.AddCarToCity(1, 0, "racecar", 2);
			Assert.IsFalse(city.CityPosition.CanMoveLeft(car2, null));

			var car3 = city.AddCarToCity(2, 0, "racecar", 2);
			Assert.IsTrue(city.CityPosition.CanMoveLeft(car3, null));

			var car4 = city.AddCarToCity(5, 0, "racecar", 2);
			Assert.IsTrue(city.CityPosition.CanMoveLeft(car4, null));
		}

	}
}
