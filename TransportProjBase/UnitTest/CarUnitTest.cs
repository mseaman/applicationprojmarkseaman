﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransportProj;

namespace UnitTest
{
	[TestClass]
	public class CarUnitTest
	{
		[TestMethod]
		public void TestMoveUp()
		{
			City city = new City(10, 10);
			var car = city.AddCarToCity(0, 9, "sedan", 1);
			car.MoveUp();
			Assert.AreEqual(9, car.YPos);

			var car2 = city.AddCarToCity(0, 8, "racecar", 2);
			car2.MoveUp();
			Assert.AreEqual(8, car2.YPos);

			var car3 = city.AddCarToCity(0, 0, "racecar", 2);
			car3.MoveUp();
			Assert.AreEqual(2, car3.YPos);
		}

		[TestMethod]
		public void TestMoveDown()
		{
			City city = new City(10, 10);
			var car = city.AddCarToCity(0, 0, "sedan", 1);
			car.MoveDown();
			Assert.AreEqual(0, car.YPos);

			var car2 = city.AddCarToCity(0, 1, "racecar", 2);
			car2.MoveDown();
			Assert.AreEqual(1, car2.YPos);

			var car3 = city.AddCarToCity(0, 5, "racecar", 2);
			car3.MoveDown();
			Assert.AreEqual(3, car3.YPos);
		}

		[TestMethod]
		public void TestMoveRight()
		{
			City city = new City(10, 10);
			var car = city.AddCarToCity(9, 0, "sedan", 1);
			car.MoveRight();
			Assert.AreEqual(9, car.XPos);

			var car2 = city.AddCarToCity(8, 0, "racecar", 2);
			car2.MoveRight();
			Assert.AreEqual(8, car2.XPos);

			var car3 = city.AddCarToCity(0, 0, "racecar", 2);
			car3.MoveRight();
			Assert.AreEqual(2, car3.XPos);
		}

		[TestMethod]
		public void TestMoveLeft()
		{
			City city = new City(10, 10);
			var car = city.AddCarToCity(0, 0, "sedan", 1);
			car.MoveLeft();
			Assert.AreEqual(0, car.XPos);

			var car2 = city.AddCarToCity(1, 0, "racecar", 2);
			car2.MoveLeft();
			Assert.AreEqual(1, car2.XPos);

			var car3 = city.AddCarToCity(5, 0, "racecar", 2);
			car3.MoveLeft();
			Assert.AreEqual(3, car3.XPos);
		}

	}
}
