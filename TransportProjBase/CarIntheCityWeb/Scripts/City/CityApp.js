﻿var CityApp = angular.module('CityApp', []);

var CityClass = function($scope, CityService) {

	this.$inject = ["$scope", "CityService"];
	$scope.vm = this;
	this.map = {};
	this.status = "";
	this.name = "Nick";
	this.carType = "Race Car";
	this.speed = 2;

	this.tick = function () {
		var self = this;
		CityService.tick()
            .success(function (good) {
				if (good) {
					self.getCityEntities();
				} else {
					self.stop();
					self.status = self.carType + " is done or stuck.";
				}
			})
            .error(function (error) {
            	self.status = 'Unable to tick: ' + error.message;
            	console.log(self.status);
            });
	}

	this.createCity = function () {
		var self = this;
		this.stop();
		this.status = "";
		CityService.createCity(self.name, self.carType, self.speed)
            .success(function () {
            	self.getCityEntities();
            })
            .error(function (error) {
            	self.status = 'Unable to create city: ' + error.message;
            	console.log(self.status);
            });
	}
	this.getCityEntities = function () {
		var self = this;
		CityService.getCityEntities()
            .success(function (cityEntities) {
            	self.map = cityEntities;
            	console.log(self.map);
            })
            .error(function (error) {
            	self.status = 'Unable to get city entities: ' + error.message;
            	console.log(self.status);
            });
	}

	this.stop = function() {
		clearInterval(this.tickInterval);
	}

	this.go = function () {
		var self = this;
		self.tickInterval = setInterval(function() {
			self.tick();
		}, 1000);
	}
	this.loadData = function () {
		this.createCity();
	}

	this.loadData();
}

CityApp.controller('CityController', CityClass);

