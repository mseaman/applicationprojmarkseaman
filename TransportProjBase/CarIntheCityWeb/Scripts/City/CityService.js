﻿CityApp.factory('CityService', ['$http', function ($http) {

	var cityService = {};

	cityService.tick = function () {
		return $http.get('/api/City/Tick/');
	};

	cityService.getCityEntities = function () {
		return $http.get('/api/City/GetEntities/');
	};

	cityService.createCity = function (name, carType, speed) {
		//return $http.post('/api/City/CreateCity/', { name: name, carType: carType, speed: speed });
		return $http({
			url: '/api/City/CreateCity/',
			method: "POST",
			params: {name: name, carType: carType, speed: speed}
		});
	};
	return cityService;

}]);