﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using TransportProj;

namespace CarIntheCityWeb.Controllers
{
    /// <summary>
    /// TODO: Need to move all this logic to business classes
    /// </summary>
	public class CityController : ApiController
    {
	    /// <summary>
	    /// Creates a new city, passenger and car based on passed in name, cartype and speed
	    /// </summary>
	    /// <param name="name">Name of the passenger</param>
	    /// <param name="carType">car type can be sedan, racecar or anything you want</param>
	    /// <param name="speed">max speed of car from 1-5</param>
		[HttpPost]
		public void CreateCity(string name, string carType, int speed)
	    {
			const int maxMaxVelocity = 5;
			//const int standardVelocity = 2;
			const string standardCarType = "Race Car";
			const int numberOfBuildings = 20;

			Random rand = new Random();
			int CityLength = 10;
			int CityWidth = 10;



			//Create City
			City myCity = new City(CityLength, CityWidth);

			//Get Car Type
		    //string carType = standardCarType;
			if (String.IsNullOrEmpty(carType))
			{
				carType = standardCarType;
			}

			
			//TODO allow user to set
			int maxVelocity = speed;
			if (maxVelocity > maxMaxVelocity) maxVelocity = maxMaxVelocity;

			//Generate location points
			var carPos = new Point(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
			var passengerStartPos = new Point(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
			var passengerDestinationPos = new Point(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

			//Generate building location points
			var buildingPositions = new List<Point>();
			for (int i = 0; i < numberOfBuildings; i++)
			{
				//rule: don't put buildings on edge of city, keep it simple
				buildingPositions.Add(new Point(rand.Next(CityLength - 2) + 1, rand.Next(CityWidth - 2) + 1));
			}

			//Be sure start and end points aren't on top of buildings.
			passengerStartPos = Utils.AdjustPositionIfNecessary(passengerStartPos, buildingPositions, rand, CityLength, CityWidth);
			passengerDestinationPos = Utils.AdjustPositionIfNecessary(passengerDestinationPos, buildingPositions, rand, CityLength, CityWidth);

			//Add car and passenger to city
			Car car = myCity.AddCarToCity(carPos.X, carPos.Y, carType, maxVelocity);
			Passenger passenger = myCity.AddPassengerToCity("Nick", passengerStartPos.X, passengerStartPos.Y,
				passengerDestinationPos.X, passengerDestinationPos.Y);

			//Add buildings to city
			for (int i = 0; i < numberOfBuildings; i++)
			{
				myCity.AddBuildingToCity(String.Format("B{0}", i), buildingPositions[i].X, buildingPositions[i].Y);
			}

			//Ok, cheesy way to simulate a singleton service
			HttpContext.Current.Cache.Remove("car");
			HttpContext.Current.Cache.Remove("passenger");
			HttpContext.Current.Cache.Add("car", car, null, DateTime.MaxValue, Cache.NoSlidingExpiration,
				CacheItemPriority.AboveNormal, null);
			HttpContext.Current.Cache.Add("passenger", passenger, null, DateTime.MaxValue, Cache.NoSlidingExpiration,
				CacheItemPriority.AboveNormal, null);

	    }

		/// <summary>
		/// Returns the entities as 2-dimensional array of names
		/// </summary>
		/// <returns>2-dimensional string array</returns>
		[HttpGet]
		public string[,] GetEntities()
		{
			Car car = (Car) HttpContext.Current.Cache["car"];

			if (car == null)
			{
				return null;
			}
			var ret = car.City.CityPosition.GetMapOfNames();
			return ret;
		}

		/// <summary>
		/// Calls Tick() and then
		/// returns the entities as 2-dimensional array of names
		/// TODO: fix to return better status
		/// </summary>
		/// <returns>2-dimensional string array</returns>
		[HttpGet]
		public bool Tick()
		{
			Car car = (Car)HttpContext.Current.Cache["car"];
			Passenger passenger = (Passenger)HttpContext.Current.Cache["passenger"];

			if (car == null)
			{
				return false;
			}

			
			return car.City.CityPosition.Tick(car, passenger);

		}

	}
}
