﻿
namespace TransportProj
{
	public class CarFactory: ICarFactory
	{
		/// <summary>
		/// Creates a car with given characteristics
		/// </summary>
		/// <param name="xPos">x</param>
		/// <param name="yPos">y</param>
		/// <param name="city">city</param>
		/// <param name="passenger">passenger (typically null at first)</param>
		/// <param name="carType">Sedan, Race Car, Renault Le Car, ...</param>
		/// <param name="maxVelocity">Number from 1 to maxMaxVelocity</param>
		/// <returns></returns>
		public Car GetCar(int xPos, int yPos, City city, Passenger passenger, string carType, int maxVelocity)
		{
			switch (carType.ToLower())
			{
				case "racecar":
				case "race car":
					return new RaceCar(xPos, yPos, city, passenger, maxVelocity);

				case "sedan":
					return new Sedan(xPos, yPos, city, passenger);

				default:
					return new GenericCar(xPos, yPos, city, passenger, carType, maxVelocity);
			}
		}
	}

}
