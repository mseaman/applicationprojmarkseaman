﻿using System;

namespace TransportProj
{
    public class Passenger : ICityEntity
    {
	    public int StartingXPos { get; private set; }
        public int StartingYPos { get; private set; }
        public int DestinationXPos { get; private set; }
        public int DestinationYPos { get; private set; }
        public Car Car { get; set; }
        public City City { get; private set; }

        public Passenger(string name, int startXPos, int startYPos, int destXPos, int destYPos, City city)
        {
	        Name = name;
			StartingXPos = startXPos;
            StartingYPos = startYPos;
            DestinationXPos = destXPos;
            DestinationYPos = destYPos;
            City = city;
        }

		//ICityEntity
		public CityEntityTypeEnum CityEntityType { get { return CityEntityTypeEnum.Passenger; } }
		public string Name { get; set; }
		public Point Position { get {return new Point(GetCurrentXPos(), GetCurrentYPos());} }
		
		
		public void GetInCar(Car car)
        {
            Car = car;
            car.PickupPassenger(this);
            Console.WriteLine("{0} got in {1}.", Name, car.CarType);

			car.SpeedUp();
        }

        public void GetOutOfCar()
        {
            Car = null;
        }

        public int GetCurrentXPos()
        {
            if(Car == null)
            {
                return StartingXPos;
            }
            else
            {
                return Car.XPos;
            }
        }

        public int GetCurrentYPos()
        {
            if (Car == null)
            {
                return StartingYPos;
            }
            else
            {
                return Car.YPos;
            }
        }

        public bool IsAtDestination()
        {
            return GetCurrentXPos() == DestinationXPos && GetCurrentYPos() == DestinationYPos;
        }
    }
}
