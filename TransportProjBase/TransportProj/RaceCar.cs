﻿using System;

namespace TransportProj
{
    public class RaceCar : Car
    {

		public RaceCar(int xPos, int yPos, City city, Passenger passenger, int maxVelocity) : base(xPos, yPos, city, passenger, "Race Car", maxVelocity)
		{
			
		}

		protected override void WritePositionToConsole()
		{
			Console.WriteLine("Race Car moved to x - {0} y - {1}", XPos, YPos);
		}
	}
}
