﻿using System;

namespace TransportProj
{
    /// <summary>
    /// Destination - not a concrete entity but something we need to track.
    /// </summary>
	public class Destination : ICityEntity
    {

        public Destination(string name, int x, int y)
        {
	        Name = name;
			Position = new Point(x, y);
        }

		//ICityEntity
		public CityEntityTypeEnum CityEntityType { get { return CityEntityTypeEnum.Destination; } }
		public string Name { get; set; }
		public Point Position { get; set; }		
		
    }
}
