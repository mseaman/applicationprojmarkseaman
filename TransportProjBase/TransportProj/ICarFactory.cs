﻿
namespace TransportProj
{
	public interface ICarFactory
	{
		Car GetCar(int xPos, int yPos, City city, Passenger passenger, string carType, int maxVelocity);
	}
}
