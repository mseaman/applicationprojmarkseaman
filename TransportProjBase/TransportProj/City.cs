﻿


namespace TransportProj
{
    public class City
    {
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }
		public CityPosition CityPosition { get; set; }

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
			CityPosition = new CityPosition();
        }

        public Car AddCarToCity(int xPos, int yPos, string carType, int maxVelocity)
        {
            ICarFactory carFactory = new CarFactory();
	        Car car = carFactory.GetCar(xPos, yPos, this, null, carType, maxVelocity);

			CityPosition.AddEntity(car);

            return car;
        }

        public Passenger AddPassengerToCity(string name, int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Passenger passenger = new Passenger(name, startXPos, startYPos, destXPos, destYPos, this);

			CityPosition.AddEntity(passenger);

			//NEW! Add new Destination
			CityPosition.AddEntity(new Destination("Dest", 
				passenger.DestinationXPos, passenger.DestinationYPos));

            return passenger;
        }

		public void AddBuildingToCity(string name, int x, int y)
		{
			Building building = new Building(name, x, y);

			CityPosition.AddEntity(building);
		}

	
	}
}
