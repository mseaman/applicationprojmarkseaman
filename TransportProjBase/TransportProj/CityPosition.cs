﻿
using System.Collections.Generic;
using System.Linq;

namespace TransportProj
{
	/// <summary>
	/// Represents the city entity positions and handles all movement logic
	/// </summary>
	public class CityPosition
    {
        protected IList<ICityEntity> CityEntities { get; set; }

	    public CityPosition()
	    {
		    CityEntities = new List<ICityEntity>();
	    }

		public void AddEntity(ICityEntity cityEntity)
		{
			CityEntities.Add(cityEntity);
		}

		public IEnumerable<ICityEntity> EntitiesInTheCity
		{
			get { return CityEntities; }
		}

		/// <summary>
		/// Gets a map of ICityEntities
		/// </summary>
		/// <returns>2 dimensional array of ICityEntity</returns>
		public ICityEntity[,] GetMap()
		{
			ICityEntity[,] map = new ICityEntity[City.XMax, City.YMax];
			foreach (var cityEntity in EntitiesInTheCity)
			{
				if (map[cityEntity.Position.X, cityEntity.Position.Y] == null 
					|| map[cityEntity.Position.X, cityEntity.Position.Y].CityEntityType != CityEntityTypeEnum.Car)
				{
					//car takes precedence
					map[cityEntity.Position.X, cityEntity.Position.Y] = cityEntity;
				}
			}
			return map;
		}

		/// <summary>
		/// Get a map of entity names
		/// </summary>
		/// <returns>2-dimensional array of strings</returns>
		public string[,] GetMapOfNames()
		{
			var map = new ICityEntity[City.XMax, City.YMax];
			var mapOfNames = new string[City.XMax, City.YMax];
			foreach (var cityEntity in EntitiesInTheCity)
			{
				if (map[cityEntity.Position.X, cityEntity.Position.Y] == null
					|| map[cityEntity.Position.X, cityEntity.Position.Y].CityEntityType != CityEntityTypeEnum.Car)
				{
					//car takes precedence
					map[cityEntity.Position.X, cityEntity.Position.Y] = cityEntity;
					mapOfNames[cityEntity.Position.X, cityEntity.Position.Y] = cityEntity.Name;
				}
			}
			return mapOfNames;
		}

		/// <summary>
		/// Logic for our "tick" one action. Move the car, pick up the passenger, slow down, move around a block.
		/// </summary>
		/// <param name="car">The car to move</param>
		/// <param name="passenger">The passenger to pickup</param>
		/// <returns>false if lost; otherwise true</returns>
		public bool Tick(Car car, Passenger passenger)
		{
			//Attempt to move the car
			bool bPassengerMoved = TryToMoveCar(car, passenger, false);

			if (!bPassengerMoved)
			{
				if (CanPickUpPassenger(car, passenger))
				{
					//this also calls car.PickupPassenger()...
					passenger.GetInCar(car);
				}
				else
				{
					if (car.Velocity > 1)
					{
						//I take a tick to slow down
						//We slow down if we are blocked too.
						car.SlowDown();
					}
					else
					{
						//only attempt to move around stuff when we are at Velocity=1
						//This keeps it simpler
						AddEntity(new AlreadyBeenHere("X", car.XPos, car.YPos));
						bPassengerMoved = TryToMoveCar(car, passenger, true); //BLOCKED!
						if (!bPassengerMoved)
						{
							//Car is lost!
							return false;
						}
					}
				}
			}

			if (bPassengerMoved)
			{
				//Utils.PassengerHitWebsite();
			}
			return true;
		}

		/// <summary>
		/// SIMPLE LOGIC to move the car around the city.  
		/// This could be much more complex and there are scenarios that will make my car get stuck.
		/// Wanted to get a basic version so I could get this working on Web page.
		/// 
		/// To improve this logic, I would create a stack of positions/choices 
		/// so I could back out of a bind and make a new choice.
		/// 
		/// Even better, I would find a path that works before sending the car off to save on drive time
		/// </summary>
		/// <param name="car">Car to move</param>
		/// <param name="passenger">Passenger to pick up and drop off</param>
		/// <param name="bBlocked">boolean specifying if we are moving around something 
		/// so we don't necessarily move toward our destination</param>
		/// <returns></returns>
		public bool TryToMoveCar(Car car, Passenger passenger, bool bBlocked)
		{
			if (ShouldMoveRight(car, passenger, bBlocked))
			{
				car.MoveRight();
				return true;
			}
			if (ShouldMoveDown(car, passenger, bBlocked))
			{
				car.MoveDown();
				return true;
			}
			if (ShouldMoveLeft(car, passenger, bBlocked))
			{
				car.MoveLeft();
				return true;
			}
			if (ShouldMoveUp(car, passenger, bBlocked))
			{
				car.MoveUp();
				return true;
			}
			return false;
		}

		/// <summary>
		/// Get all points that can block the car. 
		/// This includes AlreadyBeenHere so we don't get stuck.
		/// </summary>
		/// <returns>IEnumerable of Points</returns>
		private IEnumerable<Point> GetBlockingPositions()
		{
			return (from entity in EntitiesInTheCity
					where entity.CityEntityType == CityEntityTypeEnum.Building
					|| entity.CityEntityType == CityEntityTypeEnum.AlreadyBeenHere
					select entity.Position).ToList();
		}

		/// <summary>
		/// Checks to see if the car can move in a positive X direction.
		/// Iterates through all blocks in the city.
		/// </summary>
		/// <param name="car">Car to move</param>
		/// <param name="passenger">Passenger to pick up and deliver.</param>
		/// <returns>true if car can move; otherwise false</returns>
		public bool CanMoveRight(Car car, Passenger passenger)
		{
			if (car.XPos >= City.XMax - car.Velocity)
			{
				return false;
			}
			var positions = GetBlockingPositions();

			foreach (var position in positions)
			{
				for (var i = 1; i <= car.Velocity; i++)
				{
					if (position.Equals(new Point(car.XPos + i, car.YPos)))
					{
						return false;
					}
				}
			}
			return true;
		}

		/// <summary>
		/// Checks to see if the car SHOULD move in a positive X direction.
		/// If blocked, it may move laterally, away from the destination.
		/// </summary>
		/// <param name="car">Car to move</param>
		/// <param name="passenger">Passenger to pickup and deliver</param>
		/// <param name="bBlocked">true if the car is currently blocked and should move around something</param>
		/// <returns>true if ok to move</returns>
		public bool ShouldMoveRight(Car car, Passenger passenger, bool bBlocked)
		{
			if (bBlocked)
			{
				if (car.YPos != (!car.HasPassenger
					? passenger.StartingYPos
					: passenger.DestinationYPos))
				{
					return CanMoveRight(car, passenger);
				}
				return false;
			}
			return CanMoveRight(car, passenger) 
				&& car.XPos <
				   (!car.HasPassenger
					   ? passenger.StartingXPos - car.Velocity + 1
					   : passenger.DestinationXPos - car.Velocity + 1);
		}

		/// <summary>
		/// Checks to see if the car can move in a negative X direction.
		/// Iterates through all blocks in the city.
		/// </summary>
		/// <param name="car">Car to move</param>
		/// <param name="passenger">Passenger to pick up and deliver.</param>
		/// <returns>true if car can move; otherwise false</returns>
		public bool CanMoveLeft(Car car, Passenger passenger)
		{
			if (car.XPos < car.Velocity)
			{
				return false;
			}
			var positions = GetBlockingPositions();

			foreach (var position in positions)
			{
				for (var i = 1; i <= car.Velocity; i++)
				{
					if (position.Equals(new Point(car.XPos - i, car.YPos)))
					{
						return false;
					}
				}
			}
			return true;
		}

		/// <summary>
		/// Checks to see if the car SHOULD move in a negative X direction.
		/// If blocked, it may move laterally, away from the destination.
		/// </summary>
		/// <param name="car">Car to move</param>
		/// <param name="passenger">Passenger to pickup and deliver</param>
		/// <param name="bBlocked">true if the car is currently blocked and should move around something</param>
		/// <returns>true if ok to move</returns>
		public bool ShouldMoveLeft(Car car, Passenger passenger, bool bBlocked)
		{
			if (bBlocked)
			{
				if (car.YPos != (!car.HasPassenger
					? passenger.StartingYPos
					: passenger.DestinationYPos))
				{
					return CanMoveLeft(car, passenger);
				}
				return false;
			}
			return CanMoveLeft(car, passenger)
				&& car.XPos >
				   (!car.HasPassenger
					   ? passenger.StartingXPos + car.Velocity - 1
					   : passenger.DestinationXPos + car.Velocity - 1);
		}

		/// <summary>
		/// Checks to see if the car can move in a positive Y direction.
		/// Iterates through all blocks in the city.
		/// </summary>
		/// <param name="car">Car to move</param>
		/// <param name="passenger">Passenger to pick up and deliver.</param>
		/// <returns>true if car can move; otherwise false</returns>
		public bool CanMoveUp(Car car, Passenger passenger)
		{
			if (car.YPos >= City.YMax - car.Velocity)
			{
				return false;
			}
			var positions = GetBlockingPositions();

			foreach (var position in positions)
			{
				for (var i = 1; i <= car.Velocity; i++)
				{
					if (position.Equals(new Point(car.XPos, car.YPos + i)))
					{
						return false;
					}
				}
			}
			return true;
		}

		/// <summary>
		/// Checks to see if the car SHOULD move in a positive Y direction.
		/// If blocked, it may move laterally, away from the destination.
		/// </summary>
		/// <param name="car">Car to move</param>
		/// <param name="passenger">Passenger to pickup and deliver</param>
		/// <param name="bBlocked">true if the car is currently blocked and should move around something</param>
		/// <returns>true if ok to move</returns>
		public bool ShouldMoveUp(Car car, Passenger passenger, bool bBlocked)
		{
			if (bBlocked)
			{
				if (car.XPos != (!car.HasPassenger
					? passenger.StartingXPos
					: passenger.DestinationXPos))
				{
					return CanMoveUp(car, passenger);
				}
				return false;
			}
			return CanMoveUp(car, passenger)
				&& car.YPos <
				   (!car.HasPassenger
					   ? passenger.StartingYPos - car.Velocity + 1
					   : passenger.DestinationYPos - car.Velocity + 1);
		}

		/// <summary>
		/// Checks to see if the car can move in a negative Y direction.
		/// Iterates through all blocks in the city.
		/// </summary>
		/// <param name="car">Car to move</param>
		/// <param name="passenger">Passenger to pick up and deliver.</param>
		/// <returns>true if car can move; otherwise false</returns>
		public bool CanMoveDown(Car car, Passenger passenger)
		{
			if (car.YPos < car.Velocity)
			{
				return false;
			}
			var positions = GetBlockingPositions();

			foreach (var position in positions)
			{
				for (var i = 1; i <= car.Velocity; i++)
				{
					if (position.Equals(new Point(car.XPos, car.YPos - i)))
					{
						return false;
					}
				}
			}
			return true;
		}

		/// <summary>
		/// Checks to see if the car SHOULD move in a negative Y direction.
		/// If blocked, it may move laterally, away from the destination.
		/// </summary>
		/// <param name="car">Car to move</param>
		/// <param name="passenger">Passenger to pickup and deliver</param>
		/// <param name="bBlocked">true if the car is currently blocked and should move around something</param>
		/// <returns>true if ok to move</returns>
		public bool ShouldMoveDown(Car car, Passenger passenger, bool bBlocked)
		{
			if (bBlocked)
			{
				if (car.XPos != (!car.HasPassenger
					? passenger.StartingXPos
					: passenger.DestinationXPos))
				{
					return CanMoveDown(car, passenger);
				}
				return false;

			}
			return CanMoveDown(car, passenger)
				&& car.YPos >
				   (!car.HasPassenger
					   ? passenger.StartingYPos + car.Velocity - 1
					   : passenger.DestinationYPos + car.Velocity - 1);
		}

		/// <summary>
		/// Checks to see if the car can pick up the passenger.
		/// </summary>
		/// <param name="car">Car to do the picking up</param>
		/// <param name="passenger">Passenger to pick up</param>
		/// <returns>true if they are at the same location; otherwise false</returns>
		public bool CanPickUpPassenger(Car car, Passenger passenger)
		{
			return !car.HasPassenger && car.XPos == passenger.GetCurrentXPos() && car.YPos == passenger.GetCurrentYPos();
		}


    }
}
