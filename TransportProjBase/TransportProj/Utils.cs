﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace TransportProj
{
	public class Utils
	{
		/// <summary>
		/// Async method to get a website
		/// </summary>
		/// <param name="website">website to hit</param>
		/// <returns>Task representing the website contents in a string</returns>
		public static async Task<string> GetWebsite(string website)
		{
			HttpClient client = new HttpClient();

			Task<string> websiteContentsTask = client.GetStringAsync(website);
			string websiteContents = await websiteContentsTask;

			return websiteContents;
		}

		/// <summary>
		/// Simulates the passenger hitting the only website that matters to the world.
		/// </summary>
		public static void PassengerHitWebsite()
		{
			Task.Run(async () =>
			{
				Task<string> task = GetWebsite("http://veyo.com");

				//Console.WriteLine("Passenger hitting veyo.com.");

				string website = await task;
				//Console.WriteLine("Length of Veyo.com homepage: {0}", website.Length);
			}).Wait();
		}

		public static Point AdjustPositionIfNecessary(Point uniquePoint, List<Point> otherPoints, Random rand, int maxLength, int maxWidth)
		{
			bool bChanged = true;
			while (bChanged)
			{
				bChanged = false;
				if (otherPoints.Any(position => position.Equals(uniquePoint)))
				{
					uniquePoint = new Point(rand.Next(maxLength - 1), rand.Next(maxWidth - 1));
					bChanged = true;
				}
			}

			return uniquePoint;
		}
	}
}
