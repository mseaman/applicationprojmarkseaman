﻿using System;
using System.Collections.Generic;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
	        const int maxMaxVelocity = 5;
	        const int sedanVelocity = 1;
	        const string standardCarType = "Sedan";
			const int numberOfBuildings = 20;

			Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;
	        


            //Create City
			City myCity = new City(CityLength, CityWidth);

			//Get Car Type
			Console.WriteLine("What type of car would you like to create? Sedan? Race Car? Other? Hit Enter for standard car ({0}).", standardCarType);
			string carType = Console.ReadLine();
	        if (String.IsNullOrEmpty(carType))
	        {
				carType = standardCarType;
	        }

	        //Get max velocity
			string strMaxVelocity = null;
			if (!carType.Equals(standardCarType, StringComparison.CurrentCultureIgnoreCase))
	        {
				Console.WriteLine("How fast would you like this {0} to go on a scale from 1 to {1}?", carType, maxMaxVelocity);
				strMaxVelocity = Console.ReadLine();
			}
			//
			int maxVelocity = sedanVelocity;
	        try
	        {
				if (strMaxVelocity != null) maxVelocity = int.Parse(strMaxVelocity);
				if (maxVelocity > maxMaxVelocity) maxVelocity = maxMaxVelocity;
	        }
	        catch
	        {
				//do nothing, default to 1
	        }

			//Generate location points
			var carPos = new Point(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
			var passengerStartPos = new Point(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
			var passengerDestinationPos = new Point(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

			//Generate building location points
	        var buildingPositions = new List<Point>();
			for (int i = 0; i < numberOfBuildings; i++)
	        {
		        //rule: don't put buildings on edge of city, keep it simple
		        buildingPositions.Add(new Point(rand.Next(CityLength - 2) + 1, rand.Next(CityWidth - 2) + 1));
	        }

	        //Be sure start and end points aren't on top of buildings.
			passengerStartPos = Utils.AdjustPositionIfNecessary(passengerStartPos, buildingPositions, rand, CityLength, CityWidth);
			passengerDestinationPos = Utils.AdjustPositionIfNecessary(passengerDestinationPos, buildingPositions, rand, CityLength, CityWidth);

			//Add car and passenger to city
			Car car = myCity.AddCarToCity(carPos.X, carPos.Y, carType, maxVelocity);
			Passenger passenger = myCity.AddPassengerToCity("Nick", passengerStartPos.X, passengerStartPos.Y, 
				passengerDestinationPos.X, passengerDestinationPos.Y);

			//Add buildings to city
	        for (int i = 0; i < numberOfBuildings; i++)
	        {
				myCity.AddBuildingToCity(i.ToString(), buildingPositions[i].X, buildingPositions[i].Y);
	        }

	        //Test code
			//Car car = myCity.AddCarToCity(0, 0, carType, maxVelocity);
			//Passenger passenger = myCity.AddPassengerToCity("Nick", 3, 2, 1, 2);
			//myCity.AddBuildingToCity("Veyo", 3, 1);
			//myCity.AddBuildingToCity("Landmark", 2, 2);
			//myCity.AddBuildingToCity("XX", 4, 2);

            while(!passenger.IsAtDestination())
            {
	            if (Tick(car, passenger))
	            {
		            ShowCityPositions(myCity);
	            }
	            else
	            {
		            Console.WriteLine("{0} is LOST!", car.CarType);
					break;
	            }
            }

            return;
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        /// <returns>false if lost; otherwise true</returns>
        private static bool Tick(Car car, Passenger passenger)
        {
	        var cityPosition = car.City.CityPosition;
	        return cityPosition.Tick(car, passenger);
        }

		/// <summary>
		/// Print it out we can see the progress of each tick.
		/// </summary>
		/// <param name="myCity">The City in which we live, move, pick up and drop off.</param>
		public static void ShowCityPositions(City myCity)
		{
			//foreach (var cityEntity in myCity.CityPosition.EntitiesInTheCity)
			//{
			//	Console.WriteLine("{0} {1} current location: ({2}, {3})", 
			//		cityEntity.CityEntityType, cityEntity.Name, cityEntity.Position.X, cityEntity.Position.Y);
			//}
			//Console.WriteLine("--------------------------");
			var map = myCity.CityPosition.GetMap();
			for (int i = 0; i < City.XMax; i++)
			{
				for (int j = 0; j < City.YMax; j++)
				{
					Console.Write(ShowEntity(map[i,j]));
				}
				Console.WriteLine("");
			}
			Console.WriteLine("");
		}

	    private static string ShowEntity(ICityEntity entity)
	    {
		    if (entity == null)
		    {
			    return "_";
			}
			switch (entity.CityEntityType)
		    {
			    case CityEntityTypeEnum.AlreadyBeenHere:
				    return "X";
				case CityEntityTypeEnum.Building:
				    return "B";
				case CityEntityTypeEnum.Car:
				    return "C";
				case CityEntityTypeEnum.Destination:
				    return "D";
				case CityEntityTypeEnum.Passenger:
				    return "P";
				default:
				    return "_";
		    }
	    }

		

    }
}
