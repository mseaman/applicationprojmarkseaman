﻿
namespace TransportProj
{
	public enum CityEntityTypeEnum
	{
		Car,
		Passenger,
		Destination,
		Building,
		AlreadyBeenHere
	}
}
