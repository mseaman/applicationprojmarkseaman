﻿using System;

namespace TransportProj
{
    /// <summary>
	/// AlreadyBeenHere - ICityEntity to keep track of where we've been - we don't want to go back here.
    /// </summary>
	public class AlreadyBeenHere : ICityEntity
    {

		public AlreadyBeenHere(string name, int x, int y)
        {
	        Name = name;
			Position = new Point(x, y);
        }

		//ICityEntity
		public CityEntityTypeEnum CityEntityType { get { return CityEntityTypeEnum.AlreadyBeenHere; } }
		public string Name { get; set; }
		public Point Position { get; set; }		
		
    }
}
