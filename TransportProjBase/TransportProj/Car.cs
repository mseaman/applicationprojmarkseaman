﻿using System;

namespace TransportProj
{
    public abstract class Car : ICityEntity
    {
		protected int MaxVelocity;
	    public string CarType;
		public int Velocity;
		
		public int XPos { get; protected set; }
		public int YPos { get; protected set; }
		public Passenger Passenger { get; private set; }
        public City City { get; private set; }

        public Car(int xPos, int yPos, City city, Passenger passenger, string carType = "Sedan", int maxVelocity = 1)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
	        CarType = carType;
	        MaxVelocity = maxVelocity;
	        Velocity = maxVelocity;
        }

		//ICityEntity
		public CityEntityTypeEnum CityEntityType { get { return CityEntityTypeEnum.Car; } }
		public string Name { get { return CarType; } }
		public Point Position { get { return new Point(XPos, YPos); } }
		
		protected virtual void WritePositionToConsole()
        {
            Console.WriteLine("{0} moved to x - {1} y - {2}", CarType, XPos, YPos);
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }

		public bool HasPassenger { get { return Passenger != null; } }

		/// <summary>
		/// When we slow down, we only slow as much as we absolutely have to, so decrement by 1
		/// </summary>
		public void SlowDown()
		{
			Velocity--;
			//Console.WriteLine("{0} slowed down.", CarType);
		}
		
		/// <summary>
		/// When we speed up, we speed up as much as we can, so punch it!
		/// </summary>
		public void SpeedUp()
		{
			Velocity = MaxVelocity;
			//Console.WriteLine("{0} sped up.", CarType);
		}

		
		/// <summary>
		/// Moved all MoveXxxx() methods to the Car class since they do the same thing.
		/// </summary>
		public void MoveUp()
		{
			if (YPos < City.YMax - Velocity)
			{
				YPos += Velocity;
				//WritePositionToConsole();
			}
		}

		public void MoveDown()
		{
			if (YPos >= Velocity)
			{
				YPos -= Velocity;
				//WritePositionToConsole();
			}
		}

		public void MoveRight()
		{
			if (XPos < City.XMax - Velocity)
			{
				XPos += Velocity;
				//WritePositionToConsole();
			}
		}

		public void MoveLeft()
		{
			if (XPos >= Velocity)
			{
				XPos -= Velocity;
				//WritePositionToConsole();
			}
		}

    }
}
