﻿using System;

namespace TransportProj
{
    /// <summary>
    /// Building - keep it simple at first and only occupy one Point.
    /// </summary>
	public class Building : ICityEntity
    {

        public Building(string name, int x, int y)
        {
	        Name = name;
			Position = new Point(x, y);
        }

		//ICityEntity
		public CityEntityTypeEnum CityEntityType { get { return CityEntityTypeEnum.Building; } }
		public string Name { get; set; }
		public Point Position { get; set; }		
		
    }
}
