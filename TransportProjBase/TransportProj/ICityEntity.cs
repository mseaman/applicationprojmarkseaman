﻿using System;

namespace TransportProj
{
	/// <summary>
	/// Interface to represent any entity on the map
	/// </summary>
	public interface ICityEntity
	{
		CityEntityTypeEnum CityEntityType { get; }
		string Name { get; }
		Point Position { get; }
	}
}
