﻿
namespace TransportProj
{
    public class GenericCar : Car
    {
		public GenericCar(int xPos, int yPos, City city, Passenger passenger, string carType, int maxVelocity)
			: base(xPos, yPos, city, passenger, carType, maxVelocity)
        {
        }

    }
}
